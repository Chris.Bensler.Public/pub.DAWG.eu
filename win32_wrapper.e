-- wrapper file generated using dawg.e ver0.3a
------------------------------------------------------------
include dll_wrap.e
LINK_COMPLAIN=1

------------------------------------------------------------
-- LINKING --

-- link to the DLL's
global constant user32_dll = link_dll("user32.dll")
global constant gdi32_dll = link_dll("gdi32.dll")
global constant winmm_dll = link_dll("winmm.dll")

-- link to the c_func's
global constant LoadIcon_ = link_c_func(user32_dll, "LoadIconA", {C_POINTER,
         C_POINTER}, C_LONG)
global constant LoadCursor_ = link_c_func(user32_dll, "LoadCursorA", {C_POINTER,
         C_LONG}, C_LONG)
global constant RegisterClassEx_ = link_c_func(user32_dll, "RegisterClassExA", 
         {C_POINTER}, C_LONG)
global constant CreateWindowEx_ = link_c_func(user32_dll, "CreateWindowExA", 
         {C_LONG,C_LONG,C_LONG,C_LONG,C_LONG,C_LONG,C_LONG,C_LONG,C_LONG,C_LONG,
         C_LONG,C_LONG}, C_LONG)
global constant GetMessage_ = link_c_func(user32_dll, "GetMessageA", {C_LONG,
         C_LONG,C_LONG,C_LONG}, C_LONG)
global constant BeginPaint_ = link_c_func(user32_dll, "BeginPaint", {C_LONG,
         C_POINTER}, C_LONG)
global constant DefWindowProc_ = link_c_func(user32_dll, "DefWindowProcA", 
         {C_LONG,C_LONG,C_LONG,C_LONG}, C_LONG)
global constant GetStockObject_ = link_c_func(gdi32_dll, "GetStockObject", 
         {C_LONG}, C_LONG)

-- link to the c_proc's
global constant ShowWindow_ = link_c_proc(user32_dll, "ShowWindow", {C_LONG,C_LONG})
global constant UpdateWindow_ = link_c_proc(user32_dll, "UpdateWindow", {C_LONG})
global constant TranslateMessage_ = link_c_proc(user32_dll, "TranslateMessage", {C_LONG})
global constant DispatchMessage_ = link_c_proc(user32_dll, "DispatchMessageA", {C_LONG})
global constant GetClientRect_ = link_c_proc(user32_dll, "GetClientRect", {C_LONG,C_POINTER})
global constant DrawText_ = link_c_proc(user32_dll, "DrawTextA", {C_LONG,C_POINTER,C_LONG,C_LONG,C_LONG})
global constant EndPaint_ = link_c_proc(user32_dll, "EndPaint", {C_LONG,C_LONG})
global constant PostQuitMessage_ = link_c_proc(user32_dll, "PostQuitMessage", {C_LONG})
global constant PlaySound_ = link_c_proc(winmm_dll, "PlaySound", {C_POINTER,C_LONG,C_LONG})

------------------------------------------------------------
-- WRAPPER ROUTINES --

-----------------------------
-- c_func wrappers --

global function LoadIcon(atom arg1, object arg2)
 atom ret
 atom tmp_arg2
   if sequence(arg2) then tmp_arg2 = allocate_string(arg2) else tmp_arg2 = arg2 end if

   ret = c_func(LoadIcon_,{arg1,tmp_arg2})

   if sequence(arg2) then free(tmp_arg2) end if

   return ret
end function

global function LoadCursor(atom arg1, atom arg2)
   return c_func(LoadCursor_,{arg1,arg2})
end function

global function RegisterClassEx(atom arg1)
   return c_func(RegisterClassEx_,{arg1})
end function

global function CreateWindowEx(atom arg1, atom arg2, atom arg3, atom arg4, atom 
         arg5, atom arg6, atom arg7, atom arg8, atom arg9, atom arg10, atom 
         arg11, atom arg12)
   return c_func(CreateWindowEx_,{arg1,arg2,arg3,arg4,arg5,arg6,arg7,arg8,arg9,arg10,arg11,arg12})
end function

global function GetMessage(atom arg1, atom arg2, atom arg3, atom arg4)
   return c_func(GetMessage_,{arg1,arg2,arg3,arg4})
end function

global function BeginPaint(atom arg1, atom arg2)
   return c_func(BeginPaint_,{arg1,arg2})
end function

global function DefWindowProc(atom arg1, atom arg2, atom arg3, atom arg4)
   return c_func(DefWindowProc_,{arg1,arg2,arg3,arg4})
end function

global function GetStockObject(atom arg1)
   return c_func(GetStockObject_,{arg1})
end function


-----------------------------
-- c_proc wrappers --

global procedure ShowWindow(atom arg1, atom arg2)
   c_proc(ShowWindow_,{arg1,arg2})
end procedure

global procedure UpdateWindow(atom arg1)
   c_proc(UpdateWindow_,{arg1})
end procedure

global procedure TranslateMessage(atom arg1)
   c_proc(TranslateMessage_,{arg1})
end procedure

global procedure DispatchMessage(atom arg1)
   c_proc(DispatchMessage_,{arg1})
end procedure

global procedure GetClientRect(atom arg1, atom arg2)
   c_proc(GetClientRect_,{arg1,arg2})
end procedure

global procedure DrawText(atom arg1, object arg2, atom arg3, atom arg4, atom 
         arg5)
 atom tmp_arg2
   if sequence(arg2) then tmp_arg2 = allocate_string(arg2) else tmp_arg2 = arg2 end if

   c_proc(DrawText_,{arg1,tmp_arg2,arg3,arg4,arg5})

   if sequence(arg2) then free(tmp_arg2) end if
end procedure

global procedure EndPaint(atom arg1, atom arg2)
   c_proc(EndPaint_,{arg1,arg2})
end procedure

global procedure PostQuitMessage(atom arg1)
   c_proc(PostQuitMessage_,{arg1})
end procedure

global procedure PlaySound(object arg1, atom arg2, atom arg3)
 atom tmp_arg1
   if sequence(arg1) then tmp_arg1 = allocate_string(arg1) else tmp_arg1 = arg1 end if

   c_proc(PlaySound_,{tmp_arg1,arg2,arg3})

   if sequence(arg1) then free(tmp_arg1) end if
end procedure

