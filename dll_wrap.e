---------------------------------------------------------------------------------
-- dll_wrap.e
-- version 0.2
-- written by Chris Bensler
-- LAST MODIFIED : 03/26/02 / 20:32
---------------------------------------------------------------------------------

include rds/machine.e
include rds/dll.e

-----------------------------------------------------------------------------

global integer LINK_COMPLAIN -- if TRUE, dll_wrap.e will complain on link failures
LINK_COMPLAIN=0 -- DEFAULT

-----------------------------------------------------------------------------
global function or_all(sequence s)
 object ret
   ret = 0
   for i = 1 to length(s) do
      if sequence(s[i]) then s[i] = or_all(s[i]) end if
      ret = or_bits(ret,s[i])
   end for  
   return ret
end function

global function peek_string(atom a)
 integer i
 sequence s
    s = ""
    if a then
      i = peek(a)        
      while i do
         s = append(s, i)
         a = a + 1
         i = peek(a)
      end while
   end if
   return s
end function

-----------------------------------------------------------------------------
-- LINKING ROUTINES --

global function link_dll(sequence s)
 atom link
   link = open_dll(s)
   if link = NULL and LINK_COMPLAIN then
      puts(1,"Unable to link DLL "&s&"\n")
      while get_key()=-1 do end while
      abort(1)
   end if
   return link
end function

global function link_c_func( atom dll, sequence s, sequence args, atom result )
 atom link
   link = define_c_func( dll, s, args, result )
   if link = -1 and LINK_COMPLAIN then
      puts(1,"Unable to link to DLL function "&s&"\n")
      while get_key()=-1 do end while
      abort(1)
   end if
   return link
end function

global function link_c_proc( atom dll, sequence s, sequence args )
 atom link
   link = define_c_proc( dll, s, args )
   if link = -1 and LINK_COMPLAIN then
      puts( 1, "Unable to link to DLL procedure "&s&"\n")
      while get_key()=-1 do end while
      abort(1)
   end if
   return link
end function

global function link_c_var(atom dll, sequence s)
 atom addr
   addr = define_c_var(dll, s)
   if addr = NULL and LINK_COMPLAIN then
      puts(1,"Unable to link DLL variable "&s&"\n")
      while get_key()=-1 do end while
      abort(1)
   end if
   return addr
end function