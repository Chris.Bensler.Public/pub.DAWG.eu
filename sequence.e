---------------------------------------------------------------------------------
-- standard.e
-- written by Chris Bensler
-- LAST MODIFIED : 03/26/02 / 14:29
---------------------------------------------------------------------------------
-- commonly used routines, types and definitions
---------------------------------------------------------------------------------
without warning

---------------------------------------------------------------------------------
-- CONSTANTS --

constant TRUE=1, FALSE=0

---------------------------------------------------------------------------------
-- MISC ROUTINES --

-- returns TRUE or FALSE
-- if x is an atom, check that x >=min and x <=max
-- if x is a sequence, check that every element is in_range()
global function in_range(object x, integer min, integer max)
   if atom(x) then
      return x >= min and x <= max
   else
      for i = 1 to length(x) do
         if not in_range(x[i],min,max) then
            return FALSE
         end if
      end for
      return TRUE
   end if
end function

---------------------------------------------
-- wrappers for the builtin types, so they can be called via routine_id
type integer_(object i)
   return integer(i)
end type

type atom_(object a)
   return atom(a)
end type

type sequence_(object s)
   return sequence(s)
end type

type object_(object x)
   return object(x)
end type

global constant integer_rID = routine_id("integer_")
global constant atom_rID = routine_id("atom_")
global constant sequence_rID = routine_id("sequence_")
global constant object_rID = routine_id("object_")


---------------------------------------------------------------------------------
-- TYPES --

-- *!*SPECIAL*!* TYPE
-- this type is declared as a function because it cannot be used directly
-- it should be called from a secondary type definition
-- see type boolean_array() below
global function array(sequence s, integer type_rID)
   if length(s) then
      for i = 1 to length(s) do
         if not call_func(type_rID, {s[i]}) then
            return FALSE
         end if
      end for
   end if
   return TRUE
end function
global constant array_rID = routine_id("array")

---------------------------------------------
-- STANDARD ACCESSORY TYPES

global type boolean(object b)
   return integer(b) and (b=TRUE or b=FALSE)
end type
global constant boolean_rID = routine_id("boolean")

global type byte(object c)
   return integer(c) and in_range(c,0,255)
end type
global constant byte_rID = routine_id("byte")

global type word(object w)
   return integer(w) and in_range(w,0,65535)
end type
global constant word_rID = routine_id("word")

---------------------------------------------
-- PREDEFINED ARRAY TYPES

global type boolean_array(object s)
   return array( s, boolean_rID )
end type
global constant boolean_array_rID = routine_id("boolean_array")

global type byte_array(object s)
   return array( s, byte_rID )
end type
global constant byte_array_rID = routine_id("byte_array")

global type word_array(object s)
   return array( s, word_rID )
end type
global constant word_array_rID = routine_id("word_array")

global type integer_array(object s)
   return array( s, integer_rID )
end type
global constant integer_array_rID = routine_id("integer_array")

global type atom_array(object s)
   return array( s, atom_rID )
end type
global constant atom_array_rID = routine_id("atom_array")

global type sequence_array(object s)
   return array( s, sequence_rID )
end type
global constant sequence_array_rID = routine_id("sequence_array")

---------------------------------------------------------------------------------
-- SEQUENCE ROUTINES --

type seq_splice(object s)
   if sequence(s) and length(s)=2 and integer(s[1]) and integer(s[2]) then
      return TRUE
   else
      return FALSE
   end if
end type

global function splice(sequence seq, seq_splice indices, object x)
   return seq[1..indices[1]-1] &x& seq[indices[2]+1..length(seq)]
end function

global function dimension(integer_array dims, object init)
   for i = length(dims) to 1 by -1 do
      init = repeat(init,dims[i])
   end for
   if atom(init) then init={} end if
   return init
end function

global function flatten(object x)
 sequence ret
   if sequence(x) then
      ret = repeat(0,length(x))
      for i = 1 to length(x) do
         ret[i] = flatten(x[i])
      end for
      return ret
   end if
   return {x}
end function

-- walk the first dimension of a sequence, calling rID with args for each element
global function step(sequence s, integer rID, sequence args)
   for i = 1 to length(s) do
      s[i] = call_func(rID,{s[i]}&args)
   end for
   return s
end function

global function format(sequence s, sequence form)
   if length(s) < length(form) then
      return s & form[length(s)+1..length(form)]
   elsif length(s) > length(form) then
      return s[1..length(form)]
   end if
   return s
end function

global function find_in_range(object x, sequence s, integer pos1, integer pos2)
   for i = pos1 to pos2 do
      if equal(x,s[i]) then return i end if
   end for
   return 0
end function
