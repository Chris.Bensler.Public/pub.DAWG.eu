---------------------------------------------------------------------------------
-- dawg_err.e
-- written by Chris Bensler
-- LAST MODIFIED : 03/27/02 / 01:48
---------------------------------------------------------------------------------
-- error library for dawg.e v0.3
---------------------------------------------------------------------------------
include rds/wildcard.e
include error.e

integer first_build_exception    first_build_exception = 1

---------------------------------------------------------------------------------
-- EXCEPTION DEFINITIONS --

function ex_wrapper(object x)
   if not x[2] and atom(dir(x[1])) then
      throw_exception(x[1])
   end if
   return x
end function
global constant ex_wrapper_ID = new_exception(routine_id("ex_wrapper"))

function ex_link_list(object x)
   if sequence(x[1]) and length(x[1])=2 then
      return x
   end if
   throw_exception(x[2])
   return x
end function
global constant ex_link_list_ID = new_exception(routine_id("ex_link_list"))

-- "Name" -- len > 4
function ex_dll(object x)
   if byte_array(x[1]) and length(x[1]) > 4 then
      if platform() = LINUX then
         if equal(lower(x[1][length(x[1])-2..length(x[1])]),".so") then
            return x
         end if
      else
         if equal(lower(x[1][length(x[1])-3..length(x[1])]),".dll") then
            return x
         end if
      end if
   end if
   throw_exception(x[2])
   return x
end function
global constant ex_dll_ID = new_exception(routine_id("ex_dll"))

function ex_link(object x)
   if x[1] then
      throw_exception(x[2])
   end if
   return x
end function
global constant ex_link_ID = new_exception(routine_id("ex_link"))

function ex_file(object x)
   if not integer(x[1]) or x[1] < 0 then
      throw_exception(x[2])
   end if
   return x
end function
global constant ex_file_ID = new_exception(routine_id("ex_file"))

function ex_c_types_2_string(object x)
   if x[1] = 0 then
      throw_exception(x[2])
   end if
   return x
end function
global constant ex_c_types_2_string_ID = new_exception(routine_id("ex_c_types_2_string"))

function ex_double(object x)
   if find(x[2],x[1]) then
      throw_exception(x[3])
   end if
   return x
end function
global constant ex_double_ID = new_exception(routine_id("ex_double"))

function ex_alias(object x)
   if find(x[2],x[1]) then
      throw_exception(x[3])
   end if
   return x
end function
global constant ex_alias_ID = new_exception(routine_id("ex_alias"))

---------------------------------------------------------------------------------
-- EXCEPTION PROCESSING --
integer err_fn

procedure log_err(sequence msg)
   puts(err_fn,msg)
end procedure
set_error_handler(routine_id("log_err"))

global procedure process_exceptions(sequence name)
 sequence err
 integer pending
   pending = pending_exceptions(0)

   if pending then
      if first_build_exception then
         first_build_exception = 0
         err_fn = open("dawg.err","w")
      else
         err_fn = open("dawg.err","a")
      end if
      if err_fn =-1 then return end if

      log_err("----------------------------------------------------\n\n")
      log_err("-- " & name & " build list --\n\n")
      log_err(sprintf("%d errors encountered\n\n",pending))

      err = get_exceptions(ex_wrapper_ID)
      on_error( length(err),
         "write protection: " & name & " already exists\n\n")

      err = get_exceptions(ex_link_list_ID)
      if length(err) then
         for i = 1 to length(err) do
            log_err(sprintf("build_list[%d] not formed correctly\n",err[i]))
         end for
         log_err("\n")
      end if

      err = get_exceptions(ex_dll_ID)
      if length(err) then
         for i = 1 to length(err) do
            log_err(sprintf("build_list[%d][1]: invalid dll name\n",err[i]))
         end for
         log_err("\n")
      end if

      err  = get_exceptions(ex_link_ID)
      err &= get_exceptions(ex_c_types_2_string_ID)
      if length(err) then
         for i = 1 to length(err) do
            log_err(sprintf("build_list[%d][2][%d]: invalid link item\n",err[i]))
         end for
         log_err("\n")
      end if

      err = get_exceptions(ex_double_ID)
      if length(err) then
         for i = 1 to length(err) do
            log_err(sprintf("build_list[%d][2][%d]: duplicate link item: %s\n",err[i]))
         end for
         log_err("\n")
      end if

      err = get_exceptions(ex_alias_ID)
      if length(err) then
         for i = 1 to length(err) do
            log_err(sprintf("build_list[%d][2][%d]: link item alias already exists\n",err[i]))
         end for
         log_err("\n")
      end if

      err = get_exceptions(ex_file_ID)
      on_error( length(err),
         "*!* failed to open " & name & " for writing to *!*\n\n")

      close(err_fn)
   end if
end procedure
